type City = {
    id: number,
    name: string,
    state: string,
    country: string
}

export default City;