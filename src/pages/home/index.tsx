import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import NetInfo from '@react-native-community/netinfo';
import Geolocation from '@react-native-community/geolocation';
import {bindActionCreators} from 'redux';
import {getCity, eraseCity, changeLoadingCity} from '../../store/actions/city';
import icons from '../../utils/icons';
import {
  getWeatherFromCity,
  eraseWeather,
  changeLoadingWeather,
} from '../../store/actions/weather';
import moment from 'moment';

import {
  Container,
  SearchCity,
  ContainerWeather,
  DateLabel,
  ContainerWeatherTemperature,
  WeatherLeftContentInfo,
  WeatherRightContentInfo,
  WeatherIcon,
  Label,
  TemperatureLabel,
  LabelCelcius,
  LabelSencacao,
  LabelHumidity,
  ContainerLoading,
  Indicator,
  ContainerNoResult,
} from './styles';
import City from '../../types/City';

interface Props {
  getCity: any;
  getWeatherFromCity: any;
  eraseCity: any;
  eraseWeather: any;
  changeLoadingCity: any;
  changeLoadingWeather: any;
  loadingCity: boolean;
  loadingWeather: boolean;
  city: City;
  weather: any;
  token: string;
}


const Home: React.FC<Props> = props => {
  const [isConected, setIsConected] = useState(false);
  const [position, setPosition] = useState<any>();

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener(state => {
      setIsConected(state.isConnected ? true : false);
      //setIsConected(false);
    });
    return () => {
      unsubscribe();
    };
  }, []);

  useEffect(() => {
    if (props.weather?.data?.date) {
      const dateRegistro = moment(props.weather.data.date);
      const dataExcluir = moment(new Date()).subtract(7, 'days');
      if (dateRegistro < dataExcluir) {
        props.eraseCity();
        props.eraseWeather();
      }
    }
  }, []);

  useEffect(() => {
    if(!isConected) {
      console.log('pegou cidade Offline')
      return;
    }
    Geolocation.getCurrentPosition(
      position => {
        const initialPosition = position.coords;
        setPosition(initialPosition);
        props.changeLoadingCity();
        props.getCity(
          initialPosition.latitude,
          initialPosition.longitude,
          props.token,
        );
        console.log('pegou cidade Online')
      },
      error => console.log(error),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
  }, [isConected]);

  useEffect(() => {
    if (props.city.id && isConected) {
      props.changeLoadingWeather();
      props.getWeatherFromCity(props.city.id,props.token);
      console.log('pegou Weather Online')
    }
    else {
      console.log('pegou Weather Offline')
    }
  }, [props.city,isConected]);

  const formatDate = (date: string) => {
    return moment(date).locale('pt-br').format('DD MMM, YY');
  };

  return (
    <>
      <Container>
        {
          !props.city.id && 
          <ContainerNoResult>
            <Label>Nenhum Resultado salvo previamente</Label>
          </ContainerNoResult>
        }
        {props.city.id && (
          <SearchCity
            value={
              props.city.name ? props.city?.name + '-' + props.city.state : ''
            }
            onChangeText={() => {}}
            disabled
            editable={false}
            placeholder="Digite qual cidade"
          />
        )}
        {props.weather.id && (
          <ContainerWeather>
            <DateLabel>{formatDate(props.weather.data.date)}</DateLabel>
            <ContainerWeatherTemperature>
              <WeatherLeftContentInfo>
                <ContainerWeatherTemperature>
                  <TemperatureLabel>
                    {props.weather.data.temperature}
                  </TemperatureLabel>
                  <LabelCelcius>°C</LabelCelcius>
                </ContainerWeatherTemperature>
                <LabelSencacao>
                  Sensação Termica: {props.weather.data.sensation}
                </LabelSencacao>
              </WeatherLeftContentInfo>
              <WeatherRightContentInfo>
                <WeatherIcon
                  source={
                    icons.filter(i => i.id === props.weather.data.icon)[0].icon
                  }
                />
                <Label>{props.weather.data.condition}</Label>
              </WeatherRightContentInfo>
            </ContainerWeatherTemperature>
            <LabelHumidity>
              Umidade: {props.weather.data.humidity} %
            </LabelHumidity>
          </ContainerWeather>
        )}
      </Container>
      { (props.loadingCity || props.loadingWeather) &&
        <ContainerLoading>
            <Indicator color="green" size="large"/>
        </ContainerLoading>
      }
    </>
  );
};

function mapStateToProps(state: any) {
  return {
    token: state.tokenReducer.token,
    city: state.cityReducer.city,
    loadingCity: state.cityReducer.loading,
    loadignWeather: state.weatherReducer.loading,
    weather: state.weatherReducer.weather,
  };
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators(
    {
      getCity,
      getWeatherFromCity,
      eraseWeather,
      eraseCity,
      changeLoadingCity,
      changeLoadingWeather,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
