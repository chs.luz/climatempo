import styled from 'styled-components/native';
export const Container = styled.View`
flex: 1;
background-color: #517abd;
`
export const ContainerNoResult = styled.View`
flex: 1;
background-color: #517abd;
align-items: center;
justify-content: center;
`

export const SearchCity = styled.TextInput`
 width: 90%;
 height: 50px;
 background-color: lightgray;
 margin: 50px 20px;
 border-radius: 10px;
 padding-left: 20px;
 font-size: 25px;
 text-align: center;
`

export const ContainerWeather = styled.View`
    height: 300px;
    justify-content: center;
`

export const ContainerWeatherTemperature = styled.View`
flex-direction: row;
width: 100%;
height: 60px;
`

export const WeatherLeftContentInfo = styled.View`
flex: 1;
align-items: center;
`
export const WeatherRightContentInfo = styled.View`
flex: 1;
align-items: center;
`

export const WeatherIcon = styled.Image`
width: 70px;
height: 70px;
`

export const DateLabel = styled.Text`
font-size: 20px;
margin-left: 30px;
`

export const Label = styled.Text`
font-size: 16px;
`

export const LabelCelcius = styled.Text`
font-size: 16px;
margin-top: 15px;
`

export const LabelSencacao = styled.Text`
font-size: 16px;
margin-top: 10px;
`

export const LabelHumidity = styled.Text`
text-align: center;
margin: 100px 0px;
font-size: 16px;
`


export const TemperatureLabel = styled.Text`
font-size: 56px;
margin-left: 30px;
`

export const ContainerLoading = styled.View`
position: absolute;
right: 0;
top: 0;
left: 0;
bottom:0;
flex: 1;
align-items: center;
justify-content: center;
background-color: #517abd;
`

export const Indicator = styled.ActivityIndicator`
`