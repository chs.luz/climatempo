import { act } from 'react-test-renderer';
import City from '../../types/City';

const initialState =  {
  city: {} as City,
  loading: false,
}


interface Action {
    type: string;
    payload: {};
}

export default function(state=initialState, action: Action) {
    switch (action.type) {
        case 'CHANGE_LOADING_CITY': {
            return {...state,loading: true};
        }
        case 'CHANGE_CITY': {
            return {...state, city: action.payload, loading: false};
        }
        case 'ERROR_CHANGE_CITY': {
            return {...state, city: {},loading: false};
        }
        default: {
            return state;
        }
    }
}