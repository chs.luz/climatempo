import {combineReducers} from 'redux';

import TokenReducer from './token';
import CityReducer from './city';
import WeatherReducer from './weather';
const AppReducer = combineReducers({
    tokenReducer: TokenReducer,
    cityReducer: CityReducer,
    weatherReducer:WeatherReducer,
});

export default AppReducer;
