import { act } from 'react-test-renderer';
import City from '../../types/City';

const initialState =  {
  weather: {} as any,
  loading: false,
}


interface Action {
    type: string;
    payload: {};
}

export default function(state=initialState, action: Action) {
    switch (action.type) {
        case 'CHANGE_LOADING_WEATHER': {
            return {...state,loading: true};
        }
        case 'CHANGE_WEATHER': {
            return {...state, weather: action.payload,loading: false};
        }
        case 'ERROR_CHANGE_WEATHER': {
            return {...state, weather: {},loading: false};
        }
        default: {
            return state;
        }
    }
}