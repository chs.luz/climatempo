import api from '../../services/api';

export const getWeatherFromCity = (idCity: number, token: string) => {
    return async (dispatch: any) => {
        try {
          const response = await api.get(`/api/v1/weather/locale/${idCity}/current?token=${token}`);
          dispatch({
            type: 'CHANGE_WEATHER',
            payload: response.data,
          });
        } catch (error) {
          dispatch({
            type: 'ERROR_CHANGE_WEATHER',
            payload: {}
          });
        }
      };
}

export const changeLoadingWeather = () => {
  return {
    type: 'CHANGE_LOADING_WEATHER'
  }
}



export const eraseWeather = () => {
  return {
    type: 'CHANGE_WEATHER',
    payload: {},
  }
}
  