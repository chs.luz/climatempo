import api from '../../services/api';

export const getCity = (lat: number,lng: number, token: string) => {
    return async (dispatch: any) => {
      try {
        const response = await api.get(`/api/v1/locale/city?latitude=${lat}&longitude=${lng}&token=${token}`);
        dispatch({
          type: 'CHANGE_CITY',
          payload: response.data,
        });
      } catch (error) {
        console.log('ERROR_CHANGE_CITY: ' + JSON.stringify(error));
        dispatch({
          type: 'ERROR_CHANGE_CITY',
          payload: {}
        });
      }
    };
  };

export const changeLoadingCity = () => {
  return {
    type: 'CHANGE_LOADING_CITY'
  }
}


export const eraseCity = () => {
  return {
    type: 'CHANGE_CITY',
    payload: {},
  }
}
  