
const icons = [
    
    {id: '1', icon: require('../assets/1.png')},
    {id: '2', icon: require('../assets/2.png')},
    {id: '3', icon: require('../assets/3.png')},
    {id: '4', icon: require('../assets/4.png')},
    {id: '5', icon: require('../assets/5.png')},
    {id: '6', icon: require('../assets/6.png')},
    {id: '7', icon: require('../assets/7.png')},
    {id: '8', icon: require('../assets/8.png')},
    {id: '9', icon: require('../assets/9.png')},
  ];

export default icons;